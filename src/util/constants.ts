export class ReducerActionTypes {
	static LOGIN : string = '@ReducerActionTypes/LOGIN';
};

export class LoginReducerActionTypes{
	static LOGIN : string = '@LoginReducerActionTypes/LOGIN';
	static LOGOUT : string = '@LoginReducerActionTypes/LOGOUT';
	static TOKEN : string = '@LoginReducerActionTypes/TOKEN';
};

export class CandidatesReducerActionTypes{
	static GOT_CANDIDATES : string = '@CandidatesReducerActionTypes/CANDIDATES';
	static VOTE_CANDIDATE : string = '@CandidatesReducerActionTypes/VOTE_CANDIDATE';

};