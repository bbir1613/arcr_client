import * as React from "react";
import {candidateReducerStateInterface, candidateReducerActions} from "../reducers/candidateReducer";
import FlatButton from 'material-ui/FlatButton';

interface VoteComponentProps extends candidateReducerStateInterface, candidateReducerActions{
    toggleRoute : (routeState:any)=> void;

};

export default class VoteComponent  extends React.Component<VoteComponentProps, {}> {
	constructor(props : any){
		super(props);
	}

	render() {
		return (<div>
			Vote component
			{this.props.candidates.map((c:any) => <div key = {c.candidate.id}> {c.candidate.partid} <FlatButton primary={true} label="vote" onClick = {()=>{this.props.voteCandidate(undefined,undefined, c.candidate.id); this.props.toggleRoute({voteRoute:false,loginRoute:false,candidatesRoute:true,open:false})}}/></div>)}
			</div>)
		}
	}
        	
