import * as React from "react";
import {connect} from "react-redux"
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import {loginReducerStateInterface, loginReducerActions, LoginReducer} from "../reducers/loginReducer";
import {candidateReducerStateInterface, candidateReducerActions, CandidateReducer} from "../reducers/candidateReducer";
import LoginComponent from './LoginComponent'
import VoteComponent from './VoteComponent'
import {Pie} from 'react-chartjs-2'

interface HelloProps extends loginReducerActions, loginReducerStateInterface , candidateReducerStateInterface, candidateReducerActions{
    voteRoute:boolean;
    loginRoute:boolean;
    candidatesRoute:boolean;
    toggleRoute : (routeState:any)=> void;
};

interface HelloState {
    token:string;
};

export class Hello extends React.Component<HelloProps, HelloState> {
	constructor(props : any){
		super(props);
        this.state = {token:''}
        this.vote = this.vote.bind(this)
	}

    vote(pollid:any, userid:any, candidateid: any){
        return this.props.voteCandidate(1,this.props.user.id, candidateid)
    }
    
//{this.props.candidates.map((c:any) => <div key = {c.candidate.id}> {c.candidate.partid} : {c.votes} {c.percentage}</div>)}

    render() {
        if(this.props.candidatesRoute && this.props.candidates != undefined){
            let data = {
                labels : this.props.candidates.map((c:any) => c.candidate.partid),
                datasets : [{
                    data  : this.props.candidates.map((c:any) => c.percentage),
                            backgroundColor: ['#FF6384','#36A2EB'],
                            hoverBackgroundColor: ['#FF6384','#36A2EB']
                }]
            }
            return (<div style ={{overflow:"hidden"}}>
                    <div style ={{height:"50%", width : "60%", float:'left'}}>
                        <Pie data={data}/>
                    </div>
                <div style ={{float:'left'}}> 
                <TextField 
                    hintText="token"
                    value = {this.state.token}
                    onChange = {(event: React.FormEvent<HTMLInputElement>)=> this.setState({token: event.currentTarget.value})}
                    />
                   <FlatButton primary={true} label="Check" onClick = {()=> this.props.verify(this.state.token)}/><br/>
                    {this.props.tokenResult}
                    </div>
                    </div>)
        }
        if(this.props.loginRoute){
            return <LoginComponent tokenResult = {this.props.tokenResult} verify = {this.props.verify} error = {this.props.error} toggleRoute = {this.props.toggleRoute} user ={this.props.user} loged = {this.props.loged} login = {this.props.login} logout={this.props.logout}/> 
        }
        if(this.props.voteRoute){
            return (<div> 
                <VoteComponent candidates = {this.props.candidates} 
                               getAllCandidates={this.props.getAllCandidates}
                               voteCandidate ={this.vote}
                               toggleRoute = {this.props.toggleRoute}
                               />
                </div>)            
        }
        return <div> Loading... </div>
    }

    componentDidMount(){
            this.props.getAllCandidates(1)
        }
}

export default connect<any, any, any>(
    (state, ownProps) => {
        return {
            candidates: state.candidateReducer.candidates,
            loged : state.loginReducer.loged,
            user : state.loginReducer.user,
            voteRoute: state.loginReducer.loged,
            error : state.loginReducer.error,
            tokenResult : state.loginReducer.tokenResult,
            loginRoute: ownProps.loginRoute,
            candidatesRoute: ownProps.candidatesRoute,
            toggleRoute : ownProps.toggleRoute
        };
    }
    , dispatch => ({
        login: (username:string, password:string, toggleRoute : (routeState:any)=> void) => dispatch(LoginReducer.login(dispatch, username, password, toggleRoute)),
        logout: () => dispatch(LoginReducer.logout()),
        verify : (token:string) => dispatch(LoginReducer.verify(dispatch, token)),
        getAllCandidates : (pollid: any) => dispatch(CandidateReducer.getAllCandidates(dispatch, pollid)),
        voteCandidate: (pollid:any, userid:any, candidateid:any) => dispatch(CandidateReducer.voteCandidate(dispatch, pollid, userid, candidateid))
    }))(Hello);
