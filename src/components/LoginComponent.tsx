import * as React from "react";
import {connect} from "react-redux"
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import {loginReducerStateInterface, loginReducerActions} from "../reducers/loginReducer";

interface HelloProps extends loginReducerActions, loginReducerStateInterface{
    toggleRoute : (routeState:any)=> void;

};

interface HelloState {
	username:string;
	password:string;
};

export default class LoginComponent  extends React.Component<HelloProps, HelloState> {
	constructor(props : any){
		super(props);
		this.state = {
			username : '',
			password : ''
		};
	}

    render() {
        return (<div style = {{'textAlign':'center'}}>
        	<TextField 
        		hintText="username"
        		value = {this.state.username}
        		onChange = {(event: React.FormEvent<HTMLInputElement>)=> this.setState({username: event.currentTarget.value})}
        	/><br/>
        	<TextField 
        		hintText="password" 
        		type="password"
        		value = {this.state.password}
        		onChange = {(event: React.FormEvent<HTMLInputElement>)=> this.setState({password: event.currentTarget.value})}
        	/><br/>
        	<FlatButton primary={true} label="Login" onClick = {()=>this.props.login(this.state.username,this.state.password, this.props.toggleRoute)}/>
            {this.props.error === undefined ? '' : <div style={{color:'red'}}> {this.props.error} </div>}
        </div>)
    }
}
