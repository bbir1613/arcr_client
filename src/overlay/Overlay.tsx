import * as React from "react";
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {MuiThemeProvider, lightBaseTheme} from "material-ui/styles";
import AppBar from "material-ui/AppBar"
import IconButton  from "material-ui/IconButton"
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem'
import Hello from "../components/Hello"
import MenuIcon from 'material-ui/svg-icons/navigation/menu';

const lightMuiTheme = getMuiTheme(lightBaseTheme);

interface OverlayState{
    voteRoute:boolean;
    loginRoute:boolean;
    candidatesRoute:boolean;
    open:boolean;
}

export default class Overlay extends React.Component<any, OverlayState> {
    constructor(props:any){
        super(props);
        this.state = {
            voteRoute:false,
            loginRoute:false,
            candidatesRoute:true,
            open:false
        }
        this.toggleDrawer = this.toggleDrawer.bind(this)
        this.toggleRoute = this.toggleRoute.bind(this)
    }

    toggleDrawer(){
        // console.log("toggleDrawer")
        this.setState({open:!this.state.open})
    }

    toggleRoute(routeState:any){
        this.setState(routeState)
    }

    render() {
        return (<MuiThemeProvider muiTheme={lightMuiTheme}>
            <div>
            <AppBar title="Online voting"
            iconElementLeft={<IconButton onClick={this.toggleDrawer}><MenuIcon/></IconButton> }
            >
                <Drawer
                   docked={false}
                   width={300}
                   onRequestChange={this.toggleDrawer}
                   open={this.state.open}>
                   <MenuItem
                        primaryText = "candidates"
                        onClick ={() => this.setState({loginRoute:false,candidatesRoute:true,open:false})}
                   />
                   <MenuItem
                        primaryText = "vote"
                        onClick ={() => this.setState({loginRoute:true,candidatesRoute:false,open:false})}
                   />
                </Drawer>
            </AppBar>
            <Hello voteRoute={this.state.voteRoute} loginRoute = {this.state.loginRoute} 
                   candidatesRoute = {this.state.candidatesRoute} toggleRoute={this.toggleRoute}/>
            </div>
            </MuiThemeProvider>)
    }
}