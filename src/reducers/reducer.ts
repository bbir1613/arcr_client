import {Action} from "redux"
import {ReducerActionTypes} from "../util/constants"
import {MyAction} from "../util/utils";
import {AbstractReducer, IReducer} from "../util/reducer-utils";
import {Config} from "../util/config";

export interface reducerStateInterface {
    user:{
        id : number,
        username : string,
        password : string
    };
    loged:boolean;
}

export interface reducerActions {
    login : (username: string, password: string)=> Action;
}

export class Reducer extends AbstractReducer implements IReducer {
    initialState: reducerStateInterface;

    constructor() {
        super(ReducerActionTypes);
        // console.log("Reducer constructor");
        this.initialState = {
            user:{
                id : undefined,
                username : undefined,
                password : undefined
            },
            loged:false
        };
        console.log("Reducer constructor", this.handleActions);
    }

    reducer(state = this.initialState, action: Action) {
        return this.handleActions[action.type] ? this.handleActions[action.type](state, action) : state;
    }

    static login(dispatch: any, username: string, password: string){
        let body:FormData = new FormData();
        body.append("username", username);
        body.append("password", password);
        
        fetch(Config.SERVER_IP+'user/login' ,{
            method: 'POST',
            body:body
        })
        .then((response:any) => response.json())
        .then((response:any)=>{
            console.log(response)
            let payload:any = undefined
            if(response.error){
                payload = {
                    loged:false
                } 
            }else{
                payload = {
                    user:{
                        ...response
                    },
                    loged:true
                };
            }
            dispatch(new MyAction(ReducerActionTypes.LOGIN,...payload))
        })
        return new MyAction()
    }
}