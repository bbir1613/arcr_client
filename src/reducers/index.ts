import {combineReducers} from "redux"
import {LoginReducer} from "./loginReducer"
import {CandidateReducer} from "./candidateReducer"
import {handleReducer} from "../util/utils";

export default combineReducers({
	loginReducer: handleReducer(new LoginReducer()),
	candidateReducer: handleReducer(new CandidateReducer())
	});