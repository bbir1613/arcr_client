import {Action} from "redux"
import {LoginReducerActionTypes} from "../util/constants"
import {MyAction} from "../util/utils";
import {AbstractReducer, IReducer} from "../util/reducer-utils";
import {Config} from "../util/config";

export interface loginReducerStateInterface {
    user:{
        id : number,
        username : string,
        password : string
    };
    loged:boolean;
    error : string;
    tokenResult : string;
}

export interface loginReducerActions {
    login : (username: string, password: string, toggleRoute : (routeState:any)=> void) => Action;
    logout: () => Action;
    verify: (token:string) => Action;
}

export class LoginReducer extends AbstractReducer implements IReducer {
    initialState: loginReducerStateInterface;

    constructor() {
        super(LoginReducerActionTypes);
        // console.log("Reducer constructor");
        this.initialState = {
            user:{
                id : undefined,
                username : undefined,
                password : undefined
            },
            loged: false,
            error : undefined,
            tokenResult : undefined
        };
        // console.log("Reducer constructor", this.handleActions);
    }

    reducer(state = this.initialState, action: Action) {
        return this.handleActions[action.type] ? this.handleActions[action.type](state, action) : state;
    }

    static login(dispatch: any, username: string, password: string, toggleRoute : (routeState:any)=> void){
        let body:FormData = new FormData();
        body.append("username", username);
        body.append("password", password);
        
        fetch(Config.SERVER_IP+'user/login' ,{
            method: 'POST',
            body:body
        })
        .then((response:any) => response.json())
        .then((response:any)=>{
            console.log(response)
            let payload:any = undefined
            if(response.error){
                payload = {
                    loged:false,
                    error : response.error
                } 
            }else{
                payload = {
                    user:{
                        ...response
                    },
                    loged:true,
                    error : undefined
                };
                toggleRoute({voteRoute:true,
                        loginRoute:false,
                        candidatesRoute:false,
                        open:false});
            }
            dispatch(new MyAction(LoginReducerActionTypes.LOGIN,...payload))
        })
        return new MyAction()
    }

    static verify(dispatch: any, token: string){
        console.log("token is ",  token)
        let body:FormData = new FormData();
        body.append("token", token);
        
        fetch(Config.SERVER_IP+'vote/token' ,{
            method: 'POST',
            body:body
        })
        .then((response:any) => response.json())
        .then((response:any)=>{
            console.log(response)
            let payload:any = undefined;
            if(response.error){
                payload = {tokenResult : response.error}; 
            }else{
                payload = {tokenResult : response.token};
            }
            dispatch(new MyAction(LoginReducerActionTypes.TOKEN,...payload))
        })
        return new MyAction()
    }


    static logout(){
        const payload :any = {
            user:{
                id : undefined,
                username : undefined,
                password : undefined
            },
            loged:false
        };
        return new MyAction(LoginReducerActionTypes.LOGOUT, ...payload)
    } 
}