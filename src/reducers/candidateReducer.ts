import {Action} from "redux"
import {CandidatesReducerActionTypes} from "../util/constants"
import {MyAction} from "../util/utils";
import {AbstractReducer, IReducer} from "../util/reducer-utils";
import {Config} from "../util/config";
import {LoginReducer} from "./loginReducer"

export interface candidateReducerStateInterface {
    candidates:any;
}

export interface candidateReducerActions {
    getAllCandidates: (pollid: any) => Action;
    voteCandidate: (pollid:any, userid:any, candidateid:any) => Action;
}

export class CandidateReducer extends AbstractReducer implements IReducer {
    initialState: candidateReducerStateInterface;

    constructor() {
        super(CandidatesReducerActionTypes);
        // console.log("Reducer constructor");
        this.initialState = {
            candidates: []
        };
        // console.log("Reducer constructor", this.handleActions);
    }

    reducer(state = this.initialState, action: Action) {
        return this.handleActions[action.type] ? this.handleActions[action.type](state, action) : state;
    }

    static getAllCandidates(dispatch: any, pollid:any){
        let body:FormData = new FormData();
        body.append("id", pollid);
        
        fetch(Config.SERVER_IP+'poll/candidates' ,{
            method: 'POST',
            body:body
        })
        .then((response:any) => response.json())
        .then((response:any)=>{
            // console.log(response , CandidatesReducerActionTypes.GOT_CANDIDATES)
            dispatch(new MyAction(CandidatesReducerActionTypes.GOT_CANDIDATES, {candidates:response}))
        })
        return new MyAction()
    }

    static voteCandidate(dispatch: any, pollid:any, userid:any, candidateid:any){
        let body:FormData = new FormData();
        body.append("pollid", pollid);
        body.append("userid", userid);
        body.append("candidateid", candidateid);
        body.append("vote", "1");

        fetch(Config.SERVER_IP+'vote/vote' ,{
            method: 'POST',
            body:body
        })
        .then((response:any) => response.json())
        .then((response:any)=>{
            this.getAllCandidates(dispatch, pollid)
            dispatch(LoginReducer.logout())
        })
        return new MyAction()
    }
}