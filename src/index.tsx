import * as React from "react";
import * as ReactDOM from "react-dom";
import {Router, Route} from "react-router"
import {Provider} from "react-redux"
import {createHashHistory} from "history"

import Overlay from "./overlay/Overlay";
import store from "./store"

var injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();

ReactDOM.render((
        <Provider store={store}>
            <Router history={createHashHistory()}>
                <Route path="/" component = {Overlay}/>
            </Router>
        </Provider>
    ),
    document.getElementById("example")
);